# notification-service

Dev Challenge (Nodejs, Mongodb)

## Installation

```bash
npm install
```

## Running the app

```bash
# development
npm start
```

## Test

```bash
# unit tests
cd compose
docker-compose up -d
cd ..
npm run test

# test coverage
npm run test:cov

# lint
npm run lint

# test for debug
npm run test:debug
```

## Check Stype

```bash
# checking codding style
npm run lint
```

## Starting Example

```
cd compose
docker-compose up -d
cd ..
npm run build
npm start
```

## Go to api documentation

Start the example and go to http://localhost:3000/docs
