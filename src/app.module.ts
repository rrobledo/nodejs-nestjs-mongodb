import { ServicesModule } from 'services/services.module';
import { RepositoriesModule } from './repositories/repositories.module';
import { ControllersModule } from 'api/rest/controllers/controllers.module';
import { Module } from '@nestjs/common';
import configuration from 'config/configuration';

import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from 'utils/logger.module';

@Module({
  imports: [LoggerModule,
            RepositoriesModule,
            ServicesModule,
            ControllersModule,
            ConfigModule.forRoot({
                                    load: [configuration],
                                }),
          ]
})
export class AppModule {}
