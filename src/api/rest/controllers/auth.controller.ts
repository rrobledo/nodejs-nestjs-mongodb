import { UserInfo, CurrentUser } from './../../../services/auth/currentuser.decorator';
import { JwtAuthGuard } from './../../../services/auth/jwt-auth.guard';
import { UserDto } from './../../../model/user';
import { Controller, Get, Request, Post, UseGuards, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'services/auth/auth.service';
import { UserService } from 'services/auth/user.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('identity')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService,
              private readonly userService: UserService){
              }

  @Post('users/register')
  async register(@Body() userDto: UserDto, @Request() req) {
    return this.userService.addUser(userDto);
  }

  @UseGuards(AuthGuard('local'))
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('users/profile')
  getProfile(@CurrentUser() currentUser: UserInfo, @Request() req) {
    return this.userService.getUserByUsername(currentUser.username)
  }
}
