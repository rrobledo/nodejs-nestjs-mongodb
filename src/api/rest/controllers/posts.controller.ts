import { JwtAuthGuard } from './../../../services/auth/jwt-auth.guard';
import { Context } from './../../../model/context';
import { UserInfo, CurrentUser } from './../../../services/auth/currentuser.decorator';
import { PostsService } from './../../../services/posts.service';
import { Controller, Get, Post, Put, Body, Query, Delete, HttpCode, Param, Header, UseGuards } from '@nestjs/common';
import { PostDto } from 'model/post';
import { LoggerService } from 'nest-logger';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('posts')
@Controller()
export class PostsController {
  constructor(private readonly logger: LoggerService,
              private readonly postsService: PostsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('/posts')
  async create(@CurrentUser() currentUser: UserInfo, @Body() postDto: PostDto) {
    const context = new Context(currentUser.username);
    const postSaved = await this.postsService.addPost(context, postDto);
    this.logger.debug('new post created');
    return postSaved;
  }

  @Get('/posts/:id')
  async getPostById(@Param('id') postId = '-1') {
    return await this.postsService.getPostById(postId);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/posts/:id')
  async deletePostById(@Param('id') postId = '-1') {
    return await this.postsService.deletePost(postId);
  }

  @Get('/posts')
  async getByFilter(@Query('filter') filter = '{}', @Query('offset') offset = 0, @Query('limit') limit = 10) {
    const offsetParm = offset;
    const limitParm = limit;
    const posts = await this.postsService.getPostByFilter(filter, offsetParm, limitParm);
    return {
      pagination: {
        offset: offsetParm,
        limit: limitParm,
        total: posts.length,
      },
      data: posts,
      links: {
        next: '',
        prev: '',
      },
    };
  }
 
}
