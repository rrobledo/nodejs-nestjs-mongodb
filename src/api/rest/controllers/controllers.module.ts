import { HealthController } from './health.controller';
import { Module } from '@nestjs/common';
import { PostsController } from 'api/rest/controllers/posts.controller';
import { LoggerModule } from 'utils/logger.module';
import { ServicesModule } from 'services/services.module';
import { AuthController } from './auth.controller';
import { AuthModule } from 'services/auth/auth.module';

@Module({
  imports: [LoggerModule, ServicesModule, AuthModule],
  controllers: [HealthController, AuthController, PostsController],
  providers: [],
})
export class ControllersModule {}
