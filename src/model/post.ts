import { IsDefined } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PostDto {
    id: String;
    
    @ApiProperty()
    @IsDefined()
    name: String;

    @ApiProperty()
    @IsDefined()
    description: String;

    @ApiProperty()
    @IsDefined()
    image: String;

    @ApiProperty()
    @IsDefined()
    content : String;

    author: String;

    @ApiProperty()
    @IsDefined()
    categories: [String];

    @ApiProperty()
    @IsDefined()
    tags: [String];

    dateCreated: Date;

    datePublished: Date;

    @ApiProperty()
    @IsDefined()
    public: Boolean;
}
