import { IsDefined } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
    id: String;

    @IsDefined()
    @ApiProperty()
    name: String;

    @IsDefined()
    @ApiProperty()
    username: String;

    @IsDefined()
    @ApiProperty()
    password: String;

    dateCreated: Date;
}

export class UserDtoReduced {
    id: String;

    @IsDefined()
    name: String;

    @IsDefined()
    username: String;

    dateCreated: Date;

    constructor(id: String,
                name: String,
                username: String,
                dateCreated: Date) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.dateCreated = dateCreated;
    }

}
