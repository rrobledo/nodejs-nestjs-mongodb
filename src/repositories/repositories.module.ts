import { UsersRepositoryMongodb } from './mongodb/users.mongodb';
import { UsersRepository } from './users.repository';
import { LoggerService } from 'nest-logger';
import { MongodbModule } from './mongodb/mongodb.module';
import { PostsRepository } from './posts.repository';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PostsRepositoryMongodb } from './mongodb/posts.mongodb';


@Module({
  imports: [
    LoggerModule, 
    ConfigModule,
    MongodbModule,
  ],
  providers: [
    ConfigService,
    {
      provide: PostsRepository,
      useClass: PostsRepositoryMongodb
    },
    {
      provide: UsersRepository,
      useClass: UsersRepositoryMongodb
    },
  ],
  exports: [PostsRepository, UsersRepository],
})

export class RepositoriesModule {}
