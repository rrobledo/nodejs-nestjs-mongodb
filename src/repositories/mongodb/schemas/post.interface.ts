import { Document } from 'mongoose';

export interface Post extends Document {
    readonly name: String,
    readonly description: String,
    readonly image: String,
    readonly content : String,
    readonly author: String,
    readonly categories: [String],
    readonly tags: [String],
    readonly dateCreated: Date,
    readonly datePublished: Date,
    readonly public: Boolean
}