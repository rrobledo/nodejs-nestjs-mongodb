import { Schema } from 'mongoose';

export const PostSchema = new Schema({
    name: String,
    description: String,
    image: String,
    content : String,
    author: String,
    categories: [String],
    tags: [String],
    dateCreated: Date,
    datePublished: Date,
    public: Boolean
});
