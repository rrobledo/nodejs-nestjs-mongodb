import { Post } from './schemas/post.interface';
import { LoggerService } from 'nest-logger';
import { Injectable, Inject } from '@nestjs/common';
import { PostDto } from 'model/post';
import { PostsRepository } from 'repositories/posts.repository';
import { ConfigService } from '@nestjs/config';
import { Model } from 'mongoose';

@Injectable()
export class PostsRepositoryMongodb extends PostsRepository {
  constructor(private readonly config: ConfigService,
              private readonly logger: LoggerService,
              @Inject('POST_MODEL') private postModel: Model<Post>) {
      super();
  }

  async addPost(postDto: PostDto): Promise<PostDto> {
    postDto.dateCreated = new Date();
    const createdPost = new this.postModel(postDto);
    const post = await createdPost.save();

    return Promise.resolve(this.convertToDto(post));
  }

  async deletePost(postId: string): Promise<any> {
    return this.postModel.deleteOne({
      _id: postId
    });
  }
 
  async getPostById(postId: string): Promise<PostDto> {
    const post = await this.postModel.findOne({
                                                  _id: postId
                                                });
    if (post) {
      return this.convertToDto(post)
    } else {
      return null;
    }
  }

  async getPostByFilter(filter: string, offset: number, limit: number): Promise<PostDto[]> {
    const result = await this.postModel.find();
    const posts = result.map(post => {
      return this.convertToDto(post);
    })
    return Promise.resolve(posts)
  }

  private convertToDto(post: Post) : PostDto {
    const newPost = new PostDto();
    newPost.id = post._id;
    newPost.name = post.name;
    newPost.description = post.description;
    newPost.image = post.image;
    newPost.content = post.content;
    newPost.author = post.author;
    newPost.categories = post.categories;
    newPost.tags = post.tags;
    newPost.dateCreated = post.dateCreated;
    newPost.datePublished = post.datePublished;
    newPost.public = post.public;
    return newPost;
  } 
}
