import { UserSchema } from './schemas/user.schema';
import { PostSchema } from './schemas/post.schema';
import { LoggerService } from 'nest-logger';
import * as mongoose from 'mongoose';
import { Connection } from 'mongoose';
import { ConfigService } from '@nestjs/config';

export const mongodbProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (config: ConfigService, logger: LoggerService): Promise<typeof mongoose> => {
      const hosts = config.get('mongodb.hosts');
      const username = config.get('mongodb.username');
      const password = config.get('mongodb.password');
      const database = config.get('mongodb.database');
      const connectionUri = `mongodb://${hosts}/${database}`;

      return mongoose.connect(connectionUri, {
        user: username,
        pass: password,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    },
    inject: [ConfigService, LoggerService]
  },
  {
    provide: 'POST_MODEL',
    useFactory: (connection: Connection) => connection.model('posts', PostSchema),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'USER_MODEL',
    useFactory: (connection: Connection) => connection.model('users', UserSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
