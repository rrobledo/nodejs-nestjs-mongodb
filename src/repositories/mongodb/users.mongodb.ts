import { User } from './schemas/user.interface';
import { UsersRepository } from './../users.repository';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable, Inject } from '@nestjs/common';
import { UserDto } from 'model/user';
import { Model } from 'mongoose';

@Injectable()
export class UsersRepositoryMongodb extends UsersRepository {
  constructor(private readonly config: ConfigService,
              private readonly logger: LoggerService,
              @Inject('USER_MODEL') private userModel: Model<User>) {
    super();
  }

  async addUser(userDto: UserDto): Promise<UserDto> {
    userDto.dateCreated = new Date();
    const createdUser = new this.userModel(userDto);
    const user = await createdUser.save();

    return Promise.resolve(<UserDto> user);
  }

  async getUserByUsername(username: String): Promise<UserDto> {
    return this.userModel.findOne({
                            username: username
                          });
  }

}
