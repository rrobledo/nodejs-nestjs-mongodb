import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from './../../utils/logger.module';
import { Module } from '@nestjs/common';
import { mongodbProviders } from './mongodb.providers';

@Module({
  imports: [
    LoggerModule, 
    ConfigModule,
    MongodbModule,
  ],
  providers: [
    ...mongodbProviders,
    ConfigService,
  ],
  exports: [...mongodbProviders],
})
export class MongodbModule {}