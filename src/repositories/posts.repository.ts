import { Injectable } from '@nestjs/common';
import { PostDto } from 'model/post';

@Injectable()
export class PostsRepository {
  async addPost(postDto: PostDto): Promise<PostDto> {
    return;
  }

  async getPostById(postId: string): Promise<PostDto> {
    return;
  }

  async deletePost(postId: string): Promise<PostDto> {
    return;
  }

  async getPostByFilter(filter: string, offset: number, limit: number): Promise<PostDto[]> {
    return;
  }

}
