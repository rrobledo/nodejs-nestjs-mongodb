import { LoggerTransport } from 'nest-logger';

export default () => (
{
    port: parseInt(process.env.PORT, 10) || 3000,
    logging: {
        logLevel: process.env.LOG_LEVEL || 'debug',
        serviceName: process.env.SERVICE_NAME || 'dev-challenges',
        logAppenders: [LoggerTransport.CONSOLE, LoggerTransport.ROTATE],
        logFilePath: process.env.LOG_FILE_PATH || '/shared/logs',
    },
    mongodb: {
        hosts: process.env.MONGODB_HOSTS || 'localhost:27017',
        username: process.env.MONGODB_USER || 'admin',
        password: process.env.MONGODB_PASS || 'admin',
        database: process.env.MONGODB_DATABASE || 'blogging',
    }
   
});
