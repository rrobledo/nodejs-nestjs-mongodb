import { Context } from './../model/context';
import { PostsRepository } from './../repositories/posts.repository';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable, ConflictException, NotFoundException } from '@nestjs/common';
import { PostDto } from 'model/post';
import * as MongoParse from 'mongo-parse';

@Injectable()
export class PostsService {

  constructor(private readonly logger: LoggerService,
              private readonly config: ConfigService,
              private readonly repo: PostsRepository) {
  }

  async addPost(context: Context, postDto: PostDto) {
    postDto.author = context.username;
    return this.repo.addPost(postDto);
  }

  async getPostById(postId: string): Promise<any> {
    const postDto = await this.repo.getPostById(postId);
    if (!postDto) {
      this.logger.debug(`post with id: ${postId} not found.`);
      throw new NotFoundException(`post with id: ${postId} not found.`);
    }
    return Promise.resolve(postDto);
  }

  async deletePost(postId: string): Promise<PostDto> {
    const postDto = await this.repo.getPostById(postId);
    if (!postDto) {
      this.logger.debug(`post with id: ${postId} not found.`);
      throw new NotFoundException(`post with id: ${postId} not found.`);
    }
    return this.repo.deletePost(postId);
  }

  async getPostByFilter(filter: string, offset: number = 0, limit: number = 50): Promise<PostDto[]> {
    let filterJson = JSON.parse(filter);
    MongoParse.parse(filterJson);

    return this.repo.getPostByFilter(filter, offset, limit);
  }

}
