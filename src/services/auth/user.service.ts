import { LoggerService } from 'nest-logger';
import { UserDtoReduced } from './../../model/user';
import { UserDto } from '../../model/user';
import { UsersRepository } from '../../repositories/users.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { NONAME } from 'dns';

@Injectable()
export class UserService {
  constructor(private readonly logger: LoggerService,
              private readonly usersRepo: UsersRepository) {
  }

  async addUser(user: UserDto): Promise<any> {
    return this.usersRepo.addUser(user);
  }

  async getUserByUsername(username: String): Promise<UserDtoReduced | undefined> {
    const user = await this.usersRepo.getUserByUsername(username);
    if (!user) {
      this.logger.debug(`user ${username} not found.`);
      throw new NotFoundException(`user ${username} not found.`);
    }
    return Promise.resolve(new UserDtoReduced(user.id, user.name, user.username, user.dateCreated));
  }
}
