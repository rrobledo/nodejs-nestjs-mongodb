import { createParamDecorator } from '@nestjs/common';

export class UserInfo {
    id : String;
    username : String;
}

/**
 * retrieve the current user with a decorator
 * example of a controller method:
 * @Post()
 * someMethod(@CurrentUser() currentUser) {
 *   // do something with the user
 * }
 */
export const CurrentUser = createParamDecorator((unknown, req) : UserInfo => {
    return req.user
});
