import { RepositoriesModule } from './../../repositories/repositories.module';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LoggerModule } from './../../utils/logger.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserService } from 'services/auth/user.service';
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule} from '@nestjs/jwt';
import { jwtConstants } from './constants';

@Module({
  imports: [
    LoggerModule,
    ConfigModule,
    PassportModule,
    RepositoriesModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
      secretOrPrivateKey: jwtConstants.secret,
    }),
  ],
  providers: [UserService, AuthService, UserService, LocalStrategy, JwtStrategy, JwtAuthGuard],
  exports: [UserService, AuthService, UserService, JwtAuthGuard],
})
export class AuthModule {}
