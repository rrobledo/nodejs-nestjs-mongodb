import { UsersRepository } from './../../repositories/users.repository';
import { LoggerService } from 'nest-logger';
import { Injectable } from '@nestjs/common';
import { UserService } from 'services/auth/user.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly logger: LoggerService,
    private readonly config: ConfigService,
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly usersRepo: UsersRepository
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersRepo.getUserByUsername(username);
    if (user && user.password === pass) {
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = {
      sub: user.id,
      username: user.username
     };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
