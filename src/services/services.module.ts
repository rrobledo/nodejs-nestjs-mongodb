import { UserService } from './auth/user.service';
import { PostsService } from './posts.service';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';
import { RepositoriesModule } from 'repositories/repositories.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [LoggerModule, ConfigModule, RepositoriesModule, AuthModule],
  providers: [PostsService],
  exports: [PostsService],
})

export class ServicesModule {}
