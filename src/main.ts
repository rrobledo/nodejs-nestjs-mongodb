import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from 'utils/http-exception.filter';
import { ErrorFilter } from 'utils/error.filter';
import { ValidationPipe } from '@nestjs/common';
import { LoggerService } from 'nest-logger';

async function restBootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
  .setTitle('Dev Challenges')
  .setDescription('BLogging Service')
  .setVersion('1.0')
  .addTag('identity')
  .addTag('posts')
  .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  app.useGlobalFilters(new ErrorFilter());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.useLogger(app.get(LoggerService));
  app.enableCors();
  return app.listen(3000);
}

async function bootstrap() {
  await restBootstrap()
}
bootstrap();
