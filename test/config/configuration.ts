import { LoggerTransport } from 'nest-logger';

export default () => (
{
    port: parseInt(process.env.PORT, 10) || 3000,
    logging: {
        logLevel: process.env.LOG_LEVEL || 'debug',
        serviceName: process.env.SERVICE_NAME || 'agropago',
        logAppenders: [LoggerTransport.CONSOLE, LoggerTransport.ROTATE],
        logFilePath: process.env.LOG_FILE_PATH || '/shared/logs/agropago',
    },
    mysql: {
        type: 'mysql',
        host: process.env.DATABASE_HOST || 'localhost',
        port: process.env.DATABASE_PORT || 3306,
        username: process.env.DATABASE_USER || 'root',
        password: process.env.DATABASE_PASSWORD || 'Agropago123',
        database: process.env.DATABASE_NAME ||'agropago_sandbox',
    },
    kafka: {
        brokers: process.env.KAFKA_BROKERS || 'localhost:9092',
    },
    enterprise: {
        api_url: process.env.ENTERPRISE_API_URL || 'https://api.agrodigital.com.ar/api/v1/'
    }
   
});
