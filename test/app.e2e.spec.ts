import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from './../src/utils/logger.module';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, Inject } from '@nestjs/common';
import { HttpExceptionFilter } from 'utils/http-exception.filter';
import { ErrorFilter } from 'utils/error.filter';
import { ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import * as casual from 'casual';
import { RepositoriesModule } from 'repositories/repositories.module';
import configuration from 'config/configuration';
import { ControllersModule } from 'api/rest/controllers/controllers.module';
import { Connection } from 'mongoose';
import { existsTypeAnnotation } from '@babel/types';
const mongoose = require('mongoose');

describe('Blogging Service', () => {
  let app: INestApplication;
  let connection: Connection;

  beforeAll(async (done) => {

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [LoggerModule,
        ControllersModule,
        ConfigModule.forRoot({
                                load: [configuration],
                            }),
        RepositoriesModule
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalFilters(new ErrorFilter());
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalPipes(new ValidationPipe());
    app.enableCors();

    await app.init();
    done()
  });

  describe('End to End Test', () => {
    const name = casual.name;
    const username = casual.email;
    const password = casual.password;
    let authToken = null;
    let lastPostId = null;

    it('GET /health', (done) => {
      request(app.getHttpServer())
      .get('/health')
      .end(done)
    });

    it('POST /users/register', (done) => {
      return request(app.getHttpServer())
      .post('/users/register')
      .send(
      {
        "name": name,
        "username": username,
        "password": password
      })
      .set('Accept', 'application/json')
      .expect(201)
      .end(done)
    });

    it('POST /auth/login', (done) => {
      return request(app.getHttpServer())
      .post('/auth/login')
      .send(
      {
        "username": username,
        "password": password
      })
      .set('Accept', 'application/json')
      .expect(201)
      .then(response => {
        authToken = `Bearer ${response.body.access_token}`;
      })
      .finally(done)
    });

    it('GET /users/profile', (done) => {
      return request(app.getHttpServer())
      .get('/users/profile')
      .set('Authorization', authToken)
      .set('Accept', 'application/json')
      .send()
      .expect(200)
      .then(response => {
        expect(response.body.name = name)
        expect(response.body.username = username)
      })
      .finally(done)
    });

    it('POST /posts', (done) => {
      return request(app.getHttpServer())
      .post('/posts')
      .set('Authorization', authToken)
      .set('Accept', 'application/json')
      .send({
        "name": casual.name,
        "description": casual.description,
        "image": casual.url,
        "content" : casual.text,
        "categories": [],
        "tags": [],
        "public": true
      })
      .expect(201)
      .then(response => {
        lastPostId = response.body.id
      })
      .finally(done)
    });

    it('GET /posts/{postId}', (done) => {
      return request(app.getHttpServer())
      .get(`/posts/${lastPostId}`)
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(lastPostId === response.body.id)
      })
      .finally(done)
    });

    it('DELETE /posts/{postId}', (done) => {
      return request(app.getHttpServer())
      .delete(`/posts/${lastPostId}`)
      .set('Authorization', authToken)
      .set('Accept', 'application/json')
      .expect(200)
      .end(done)
    });

    it('GET /posts/{postId}', (done) => {
      return request(app.getHttpServer())
      .get(`/posts/${lastPostId}`)
      .set('Accept', 'application/json')
      .expect(404)
      .end(done)
    });

    it('GET /posts', (done) => {
      return request(app.getHttpServer())
      .get('/posts')
      .set('Authorization', authToken)
      .set('Accept', 'application/json')
      .expect(200)
      .end(done)
    });

    afterAll(async (done) => {
      await app.close();
      done()
    });

  });
});
